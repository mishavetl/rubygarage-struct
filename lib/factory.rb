
require_relative 'base_factory.rb'

module Factory
  def self.new(*args, &block)
    class_ = create_class(args, block)
    check_args(args)
    return class_
  end

  private
  
  def self.check_args(args)
    for arg in args
      if not arg.is_a? Symbol
        raise NameError, 'key must be a constant'
      end
    end
  end

  def self.create_class(args, block)
    class_ = nil
    if args[0].is_a? String
      name = args[0]
      args.shift
      class_ = Factory.const_set(name.intern, Class.new(BaseFactory, &block))
    else
      class_ = Class.new(BaseFactory, &block)
    end
    class_.arg_names = args
    class_
  end
end

