Dir[__dir__ + '/modules/*.rb'].each {|file| require file }

class BaseFactory
  include FactoryValues
  include FactoryPresenters
  include FactoryIterators
  include FactoryIndexers
  include FactoryComparators

  def initialize(*args)
    check_args(args)
    args.zip(self.members).each do |arg, arg_name|
      instance_variable_set("@#{arg_name}", arg)
    end
  end
  
  def self.arg_names=(names)
    @@arg_names = names
    attr_accessor(*names)
  end

  def check_args(args)
    if args.length != members.length
      raise ArgumentError, "wrong number of arguments: #{members} needed"
    end
  end
  
  def members
    @@arg_names
  end

  def size
    members.size
  end

  def length
    size
  end

  alias :length :size
end
