module FactoryPresenters
  def to_h
    Hash[each_pair]
  end

  def inspect
    '#<struct ' + self.class.to_s + ' ' + values.join(", ") + '>'
  end

  def hash
    to_h.merge({" class_name": self.class.to_s}).hash
  end

  alias :to_s :inspect
end
