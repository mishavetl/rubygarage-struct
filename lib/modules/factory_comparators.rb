module FactoryComparators
  def eql?(other)
    hash == other.hash
  end

  alias :== :eql?
end
