module FactoryIterators
  def each_with_nullable_block(vals, block)
    if !block
      vals
    else
      vals.each &block
    end
  end

  def each(&block)
    vals = values
    each_with_nullable_block(vals, block)
  end

  def each_pair(&block)
    vals = members.zip(values)
    each_with_nullable_block(vals, block)
  end

  def select(&block)
    if !block
      values
    else
      values.select &block
    end
  end
end
