module FactoryValues
  def values
    members.map { |name| send(name.to_sym) }
  end

  alias :to_a :values
end
