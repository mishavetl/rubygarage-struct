module FactoryIndexers
  def [](key)
    if key.is_a? Integer
      instance_variable_get("@#{members[key]}")
    else
      instance_variable_get("@#{key.intern.to_sym}")
    end
  end

  def []=(key, value)
    if key.is_a? Integer
      instance_variable_set("@#{members[key]}", value)
    else
      instance_variable_set("@#{key.intern.to_sym}", value)
    end
  end

  def values_at(*args)
    vals = []
    for arg in args
      if arg.is_a? Integer
        vals.push values[arg]
      else
        vals += values[arg]
      end
    end
    vals
  end

  def dig(*indexes)
    value = to_h[indexes[0]]
    if not value
      return nil
    end
    indexes.shift
    value.dig(*indexes)
  end
end
